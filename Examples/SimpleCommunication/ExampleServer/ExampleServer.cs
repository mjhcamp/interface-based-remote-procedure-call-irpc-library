﻿using IRPC;
using IRPC.TCP;
using System;
using System.Collections.Generic;
using System.Net;

namespace Examples.SimpleCommunication
{
    public class ExampleServer : IRemotePrinterRPC
    {
        private IList<IInterfacingBridge<IRemotePrinterRPC>> _remoteBridges;
        private IList<IInterfacingBridge> _localBridges;

        private IInterfacingServer _server;

        public ExampleServer()
        {
            _remoteBridges = new List<IInterfacingBridge<IRemotePrinterRPC>>();
            _localBridges = new List<IInterfacingBridge>();
        }

        public void Start(int port)
        {
            // Create an uri specifying the host port and address for the interfacing server
            // Use uri header net.tcp since we are using the TCP implementation
            var uriBuilder = new UriBuilder("net.tcp", "127.0.0.1", port);
            _server = new TCPInterfacingServer();

            // Register the local side of the interface (this 'IRemotePrinterRPC')
            _server.RegisterInterface<IRemotePrinterRPC>(this, LocalConnected);

            // Register a listener for when a remote interface of the type 'IRemotePrinterRPC' connected
            _server.RegisterInterface<IRemotePrinterRPC>(RemoteConnected);

            // Start server
            _server.StartServer(uriBuilder.Uri);
        }

        public void Stop()
        {
            _server.StopServer();
            foreach (var remote in _remoteBridges)
                remote.Disconnect();

            foreach (var local in _localBridges)
                local.Disconnect();
        }

        public void Print(string text)
        {
            Console.WriteLine(text);
        }

        public void PrintRemote(string text)
        {
            foreach (var remote in _remoteBridges)
                remote.RemoteInterface.Print(text);
        }

        /// <summary>
        /// A remote application providing the IRemotePrinterRPC interface has connected to this application
        /// and can now be used for remote printing
        /// </summary>
        /// <param name="remoteBridge"></param>
        private void RemoteConnected(IInterfacingBridge<IRemotePrinterRPC> remoteBridge)
        {
            _remoteBridges.Add(remoteBridge);
            remoteBridge.Disconnected += OnDisconnect;
        }

        /// <summary>
        /// A remote application has connected and can now use the IRemotePrinterRPC interface provided by this
        /// application
        /// </summary>
        /// <param name="localBridge"></param>
        private void LocalConnected(IInterfacingBridge localBridge)
        {
            _localBridges.Add(localBridge);
            localBridge.Disconnected += OnDisconnect;
        }


        private void OnDisconnect(object sender, DisconnectedEventArgs e)
        {
            if (e.Exception == null)
                Console.WriteLine("Remote application closed a connection");
            else
            {
                Console.WriteLine("A connection was closed due to an exception: ");
                Console.WriteLine(e.Exception.Message);
            }

            var bridge = sender as IInterfacingBridge<IRemotePrinterRPC>;
            if(bridge == null)
            {
                _localBridges.Remove(sender as IInterfacingBridge);
            } 
            else
                _remoteBridges.Remove(bridge);
        }
    }
}
