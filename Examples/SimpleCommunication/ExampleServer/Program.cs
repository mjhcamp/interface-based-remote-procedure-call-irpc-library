﻿using System;
using System.Net;
namespace Examples.SimpleCommunication
{
    class Program
    {
        static void Main()
        {
            var server = new ExampleServer();
            // For this example to work, make sure that the port entered here is available on your pc
            // and matches the port in the ExampleClient project
            const int hostPort = 5431;

            server.Start(hostPort);

            while (true)
            {
                var input = Console.ReadLine();
                if (input.ToLowerInvariant() == "exit")
                {
                    server.Stop();
                    return;
                }

                server.PrintRemote(input);
            }
        }
    }
}
