﻿namespace Examples.SimpleCommunication
{
    public interface IRemotePrinterRPC
    {
        void Print(string text);
    }
}
