﻿using IRPC;
using IRPC.TCP;
using System;
using System.Net;

namespace Examples.SimpleCommunication
{
    public class ExampleClient : IRemotePrinterRPC
    {
        private IInterfacingBridge<IRemotePrinterRPC> _remoteBridge;
        private IInterfacingBridge _localBridge;

        public void Connect(IPEndPoint endpoint)
        {
            // Create a uri to which the interfacing client should connect
            // Use uri header net.tcp since we are using the TCP implementation
            var uriBuilder = new UriBuilder("net.tcp", endpoint.Address.ToString(), endpoint.Port);                    
            var client = new TCPInterfacingClient();

            // Register the local side of the interface (this 'IRemotePrinterRPC')
            client.RegisterInterface<IRemotePrinterRPC>(this, LocalConnected);

            // Register a listener for when a remote interface of the type 'IRemotePrinterRPC' connected
            client.RegisterInterface<IRemotePrinterRPC>(RemoteConnected);

            client.Connect(uriBuilder.Uri);
        }

        public void Disconnect()
        {
            _remoteBridge.Disconnect();
            _localBridge.Disconnect();
        }

        public void Print(string text)
        {
            Console.WriteLine(text);
        }       

        public void PrintRemote(string text)
        {
            _remoteBridge.RemoteInterface.Print(text);
        }

        /// <summary>
        /// A remote application providing the IRemotePrinterRPC interface has connected to this application
        /// and can now be used for remote printing
        /// </summary>
        /// <param name="remoteBridge"></param>
        private void RemoteConnected(IInterfacingBridge<IRemotePrinterRPC> remoteBridge)
        {
            _remoteBridge = remoteBridge;
            _remoteBridge.Disconnected += OnDisconnect;
        }

        /// <summary>
        /// A remote application has connected and can now use the IRemotePrinterRPC interface provided by this
        /// application
        /// </summary>
        /// <param name="localBridge"></param>
        private void LocalConnected(IInterfacingBridge localBridge)
        {
            _localBridge = localBridge;
            _localBridge.Disconnected += OnDisconnect;
        }

        private void OnDisconnect(object sender, DisconnectedEventArgs e)
        {
            if (e.Exception == null)
                Console.WriteLine("Remote application closed the connection");
            else
            {
                Console.WriteLine("Connection was closed due to an exception: ");
                Console.WriteLine(e.Exception.Message);
            }
        }
    }
}
