﻿using IRPC;
using System;
namespace Examples.SimpleCommunication
{
    public interface IDataSinkRPC
    {
        void ReportId(Guid id);

        [RemotingTimeout(5000)]
        void UpdateData(Guid id, float[] data);
    }
}
