﻿using IRPC;
using IRPC.Serializers;
using IRPC.TCP;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Examples.SimpleCommunication
{
    public class ExampleClient
    {
        private IInterfacingBridge<IDataSinkRPC> _remoteBridge;
        private AutoResetEvent _connectedEvent;
        private Guid _id;

        public void Connect(IPEndPoint endpoint)
        {
            // Create a uri to which the interfacing client should connect
            // Use uri header net.tcp since we are using the TCP implementation
            var uriBuilder = new UriBuilder("net.tcp", endpoint.Address.ToString(), endpoint.Port);
            var client = new TCPInterfacingClient();
            _connectedEvent = new AutoResetEvent(false);
            
            // Register a listener for when a remote interface of the type 'IRemotePrinterRPC' connected
            client.RegisterInterface<IDataSinkRPC>(RemoteConnected);

            // Register (high-performance) array serializer 
            // (from the IRPC.Serializers package)
            // You can create you're own serializers for special types by implementing the IInterfacingSerializer interface
            client.RegisterSerializer<Array, ArraySerializer>();
                        
            client.Connect(uriBuilder.Uri);
        }

        public void Disconnect()
        {
            _remoteBridge.Disconnect();
        }

        public void AwaitConnection()
        {
            _connectedEvent.WaitOne();
        }

        public bool TransferData(float[] data)
        {
            try
            {
                _remoteBridge.RemoteInterface.UpdateData(_id, data);
                return true;
            } 
            catch(DisconnectedException)
            {
                return false;
            }
            catch(TimeoutException)
            {
                return false;
            }
        }

        /// <summary>
        /// A remote application providing the IRemotePrinterRPC interface has connected to this application
        /// and can now be used for remote printing
        /// </summary>
        /// <param name="remoteBridge"></param>
        private void RemoteConnected(IInterfacingBridge<IDataSinkRPC> remoteBridge)
        {
            _remoteBridge = remoteBridge;
            _remoteBridge.RemoteInterface.ReportId(_id = Guid.NewGuid());
            _remoteBridge.Disconnected += OnDisconnect;
            _connectedEvent.Set();
        }

        private void OnDisconnect(object sender, DisconnectedEventArgs e)
        {
            if (e.Exception == null)
                Console.WriteLine("Remote application closed the connection");
            else
            {
                Console.WriteLine("Connection was closed due to an exception: ");
                Console.WriteLine(e.Exception.Message);
            }
        }
    }
}
