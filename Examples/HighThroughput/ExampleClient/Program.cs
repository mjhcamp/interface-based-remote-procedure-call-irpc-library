﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Examples.SimpleCommunication
{
    class Program
    {
        static void Main()
        {
            var client = new ExampleClient();
            // For this example to work, make sure that the port entered here is available on your pc
            // and matches the port in the ExampleServer project
            var remoteEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"),5431);

            while(true)
            {
                try
                {
                    client.Connect(remoteEndPoint);
                    break;
                } 
                catch(SocketException e)
                {
                    Console.WriteLine("E" + e.ErrorCode + ": " + e.Message);
                    Console.WriteLine("Make sure the 'ExampleServer' application is up and running");
                    Thread.Sleep(1000);
                }
            }
                        
            // Generate payload
            var random = new Random(DateTime.UtcNow.Millisecond);
            var payload = new float[512*1024].Select(x=>(float)random.NextDouble()).ToArray();

            client.AwaitConnection();

            while(true)
            {
                if (!client.TransferData(payload))
                    return;
            }
        }
    }
}
