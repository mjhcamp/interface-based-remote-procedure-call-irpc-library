﻿using IRPC;
using IRPC.Serializers;
using IRPC.TCP;
using System;
using System.Collections.Generic;
using System.Net;

namespace Examples.SimpleCommunication
{
    public class ExampleServer : IDataSinkRPC
    {
        private IList<IInterfacingBridge> _localBridges;
        private IInterfacingServer _server;
        private Dictionary<Guid, KeyValuePair<DateTime,long>> _receivedData;

        public ExampleServer()
        {
            _localBridges = new List<IInterfacingBridge>();
            _receivedData = new Dictionary<Guid, KeyValuePair<DateTime, long>>();
        }

        public void Start(int port)
        {
            // Create an uri specifying the host port and address for the interfacing server
            // Use uri header net.tcp since we are using the TCP implementation
            var uriBuilder = new UriBuilder("net.tcp", "127.0.0.1", port);
            _server = new TCPInterfacingServer();

            // Register the local side of the interface (this 'IRemotePrinterRPC')
            _server.RegisterInterface<IDataSinkRPC>(this, LocalConnected);
            
            // Register (high-performance) array serializer 
            // (from the IRPC.Serializers package)
            // You can create you're own serializers for special types by implementing the IInterfacingSerializer interface
            _server.RegisterSerializer<Array, ArraySerializer>();
            
            // Start server
            _server.StartServer(uriBuilder.Uri);
        }

        public void Stop()
        {
            _server.StopServer();
            foreach (var local in _localBridges)
                local.Disconnect();
        }

        public void ReportId(Guid id)
        {
            _receivedData.Add(id, new KeyValuePair<DateTime, long>(DateTime.UtcNow,0));
        }

        public void UpdateData(Guid id, float[] data)
        {
            var kvp = _receivedData[id];
            _receivedData[id] = new KeyValuePair<DateTime, long>(kvp.Key, kvp.Value + data.LongLength);

            var rate = (kvp.Value * sizeof(float)) / (((DateTime.UtcNow - kvp.Key)).TotalSeconds * 1024 * 1024);
            Console.WriteLine("{0:N2} MB/s transfer rate from {1}", rate, id);
        }
        
        /// <summary>
        /// A remote application has connected and can now use the IRemotePrinterRPC interface provided by this
        /// application
        /// </summary>
        /// <param name="localBridge"></param>
        private void LocalConnected(IInterfacingBridge localBridge)
        {
            _localBridges.Add(localBridge);
            localBridge.Disconnected += OnDisconnect;
        }
        
        private void OnDisconnect(object sender, DisconnectedEventArgs e)
        {
            if (e.Exception == null)
                Console.WriteLine("Remote application closed a connection");
            else
            {
                Console.WriteLine("A connection was closed due to an exception: ");
                Console.WriteLine(e.Exception.Message);
            }

            var bridge = sender as IInterfacingBridge;
            if (bridge == null)
            {
                _localBridges.Remove(bridge);
            }
        }
    }
}
