﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;

namespace IRPC.Serializers
{
    [Serializable]
    internal class ArrayMetaData
    {
        internal ArrayMetaData(Array array)
        {
            ElementTypeName = array.GetType().GetElementType().AssemblyQualifiedName;
            Dimensions = new int[array.Rank].Select((x, i) => array.GetLength(i)).ToArray();
        }

        internal string ElementTypeName { get; set; }

        internal Type ElementType { get { return Type.GetType(ElementTypeName); } }

        internal int[] Dimensions { get; set; }
    }

    public class ArraySerializer : IInterfacingSerializer<Array>
    {
        [DllImport("Kernel32.dll", EntryPoint = "RtlMoveMemory")]
        private static extern void MoveMemory(IntPtr destination, IntPtr source, int length);

        [DllImport("Kernel32.dll")]
        private static extern void CopyMemory(IntPtr destination, IntPtr source, int length);

        public ArraySegment<byte> Serialize(Array value)
        {
            var formatter = new BinaryFormatter();
            byte[] metaDataBytes;
            using (var s = new MemoryStream())
            {
                var metaData = new ArrayMetaData(value);
                formatter.Serialize(s, metaData);
                metaDataBytes = s.ToArray();
            }

            var elementSize = Marshal.SizeOf(value.GetType().GetElementType());
            var returnValue = new byte[value.Length * elementSize + metaDataBytes.Length];
            Array.Copy(metaDataBytes, returnValue, metaDataBytes.Length);

            var valueHandle = GCHandle.Alloc(value, GCHandleType.Pinned);
            var returnHandle = GCHandle.Alloc(returnValue, GCHandleType.Pinned);

            CopyMemory(
                new IntPtr(returnHandle.AddrOfPinnedObject().ToInt64() + metaDataBytes.Length),
                valueHandle.AddrOfPinnedObject(),
                value.Length * elementSize);

            valueHandle.Free();
            returnHandle.Free();
            return new ArraySegment<byte>(returnValue);
        }

        public Array Deserialize(ArraySegment<byte> data)
        {
            var formatter = new BinaryFormatter();

            ArrayMetaData metaData;
            int offset;
            using (var s = new MemoryStream(data.Array,data.Offset,data.Count))
            {
                metaData = (ArrayMetaData)formatter.Deserialize(s);
                offset = (int)s.Position + data.Offset;
            }

            var elementSize = Marshal.SizeOf(metaData.ElementType);
            var returnValue = Array.CreateInstance(metaData.ElementType, metaData.Dimensions);

            var valueHandle = GCHandle.Alloc(data.Array, GCHandleType.Pinned);
            var returnHandle = GCHandle.Alloc(returnValue, GCHandleType.Pinned);

            CopyMemory(
                returnHandle.AddrOfPinnedObject(),
                new IntPtr(valueHandle.AddrOfPinnedObject().ToInt64() + offset),
                returnValue.Length * elementSize);

            valueHandle.Free();
            returnHandle.Free();
            return returnValue;
        }
    }
}
