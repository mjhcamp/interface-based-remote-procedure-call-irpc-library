﻿using System;
using System.Linq;
using IRPC.Serializers;
using IRPC;
using NUnit.Framework;

namespace Serializers.Test
{
    [TestFixture]
    public class ArraySerializerTests
    {
        private IInterfacingSerializer<Array> _serializer;

        [SetUp]
        public void Initialize()
        {
            _serializer = new ArraySerializer();
        }
        
        [Test]
        public void DimensionTest([Range(1,16)]int dim, [Range(1,3)]int size)
        {
            // Create arrays of increasingly high dimension
            var dimension = new int[dim].Select(x => size).ToArray();
            var array = Array.CreateInstance(typeof(byte), dimension, new int[dim]);

            // Serialize
            var serialized = _serializer.Serialize(array);

            // Deserialize
            var deserialized = _serializer.Deserialize(serialized);

            // Check dimensions of output array
            Assert.AreEqual(deserialized.Rank, array.Rank);
            for(var i = 0;i<array.Rank;i++)
            {
                Assert.AreEqual(deserialized.GetLength(i), array.GetLength(i));
            }            
        }

        [Test]
        public void ValueTest([Range(128,1024,32)]int size)
        {           
            // Create arrays of fixed size with random data
            var random = new Random(DateTime.UtcNow.Millisecond);

            var array = new float[size].Select(x => (float)random.NextDouble()).ToArray();

            // Serialize
            var serialized = _serializer.Serialize(array);

            // Deserialize
            var deserialized = _serializer.Deserialize(serialized) as float[];

            Assert.IsNotNull(deserialized);

            for (var j = 0; j < size; j++)
            {
                Assert.AreEqual(deserialized[j], array[j]);
            }
        }
    }
}
