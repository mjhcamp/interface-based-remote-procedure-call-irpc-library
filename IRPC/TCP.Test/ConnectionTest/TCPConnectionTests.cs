﻿using NUnit.Framework;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace IRPC.TCP.Test.ConnectionTest
{
    [TestFixture]
    public class TCPConnectionTests : TCPTestBase<IConnectionTestServer, ConnectionTestObject>
    {
        private const int Port = 1337;
        private const int TwoWayRemoteProcedureRegistrationTimeout = 300;
        
        private readonly Func<object, bool> _waitAction = x =>
        {
            var resetEvent = (AutoResetEvent)x;
            return resetEvent.WaitOne(TwoWayRemoteProcedureRegistrationTimeout);
        };

        [SetUp]
        public override void Initialize()
        {
            base.Initialize();
        }

        [Test]
        public void SetupTearDownTest([Range(10,100,10)]int setupTearDownCount)
        {
            for (int i = 0; i < setupTearDownCount; i++)
            {
                try
                {
                    // Start server
                    Server.StartServer(new UriBuilder("net.tcp", "127.0.0.1", Port).Uri);
                }
                catch (Exception e)
                {
                    Assert.Fail("An exception was thrown while starting the server: " + e.Message + "\n\n" + e.StackTrace);
                }

                try
                {
                    // Start server
                    Server.StopServer();
                }
                catch (Exception e)
                {
                    Assert.Fail("An exception was thrown while stopping the server: " + e.Message + "\n\n" + e.StackTrace);
                }

                // Check if the socket was released properly
                var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                try
                {
                    socket.Bind(new IPEndPoint(IPAddress.Any, Port));
                } 
                catch(SocketException e)
                {
                    if (e.SocketErrorCode == SocketError.AddressAlreadyInUse)
                    {
                        Assert.Fail("Server failed to release the socket after stopping");
                    }
                    else
                        throw;
                }

                socket.Close();
            }            
        }
           
        [Test]
        public void OneWayRemoteProcedureRegistrationServerTest()
        {
            var serverSideCallback = new AutoResetEvent(false);
            
            // Register interface
            var server = new ConnectionTestObject(serverSideCallback);
            Server.RegisterInterface<IConnectionTestClient>(server.OnConnectionReceived);

            var client = new ConnectionTestObject(null);
            Client.RegisterInterface<IConnectionTestClient>(client, bridge => { });

            // Start both
            Server.StartServer(new UriBuilder("net.tcp", "127.0.0.1", Port).Uri);
            Client.Connect(new UriBuilder("net.tcp", "127.0.0.1", Port).Uri);

            // Wait for completion
            var serverComplete = Task.Factory.StartNew(_waitAction, serverSideCallback);
            serverComplete.Wait();

            Assert.IsTrue(serverComplete.Result);
            Server.StopServer();
        }

        [Test]
        public void OneWayRemoteProcedureRegistrationClientTest()
        {
            var clientSideCallback = new AutoResetEvent(false);

            // Register interface
            var server = new ConnectionTestObject(null);
            Server.RegisterInterface<IConnectionTestServer>(server, bridge => { });

            var client = new ConnectionTestObject(clientSideCallback);
            Client.RegisterInterface<IConnectionTestServer>(client.OnConnectionReceived);

            // Start both
            Server.StartServer(new UriBuilder("net.tcp", "127.0.0.1", Port).Uri);
            Client.Connect(new UriBuilder("net.tcp", "127.0.0.1", Port).Uri);

            // Wait for completion
            var serverComplete = Task.Factory.StartNew(_waitAction, clientSideCallback);
            serverComplete.Wait();

            Assert.IsTrue(serverComplete.Result);
            Server.StopServer();
        }

        [Test]
        public void Timeout500Test([Range(150, 950, 100)]int sleepTime, [Values(500)]int timeout)
        {
            // Setup
            IInterfacingBridge<IConnectionTestServer> clientBridge;
            IInterfacingBridge serverBridge;
            SetupServerConnection(Port, out clientBridge, out serverBridge);
            Server.StopServer();

            // First check if the method returns in time
            try
            {
                clientBridge.RemoteInterface.Timeout500(sleepTime);
            }
            catch (TimeoutException)
            {
                if (sleepTime < timeout)
                    Assert.Fail("Method timed out");
            }

            clientBridge.Disconnect();
            serverBridge.Disconnect();
        }
        
        [Test]
        public void Timeout1000Test([Range(550, 1450, 100)]int sleepTime, [Values(1000)]int timeout)
        {
            // Setup
            IInterfacingBridge<IConnectionTestServer> clientBridge;
            IInterfacingBridge serverBridge;
            SetupServerConnection(Port, out clientBridge, out serverBridge);
            Server.StopServer();

            // First check if the method returns in time
            try
            {
                clientBridge.RemoteInterface.Timeout1000(sleepTime);
            }
            catch (TimeoutException)
            {
                if(sleepTime < timeout)
                    Assert.Fail("Method timed out");
            }

            clientBridge.Disconnect();
            serverBridge.Disconnect();
        }

        [Test]
        public void TimeoutDefaultTest([Values(2000)]int defaultTimeout, [Values(1000)]int timeout)
        {
            // Setup
            IInterfacingBridge<IConnectionTestServer> clientBridge;
            IInterfacingBridge serverBridge;
            SetupServerConnection(Port, out clientBridge, out serverBridge);
            Server.StopServer();

            clientBridge.MethodTimeout = defaultTimeout;
            var sleeptime = (defaultTimeout + timeout)/2;

            // Check if the custom timeout method still times out at its designated timeout
            bool timedOut = false;
            try
            {
                clientBridge.RemoteInterface.Timeout1000(sleeptime);
            }
            catch (TimeoutException)
            {
                timedOut = true;
            }
            if (!timedOut)
                Assert.Fail("Method did not timeout as specified by its remotingtimeout attribute");

            // Check if the default timeout method completes correctly
            timedOut = false;
            try
            {
                clientBridge.RemoteInterface.TimeoutDefault(sleeptime);
            }
            catch (TimeoutException)
            {
                timedOut = true;
            }
            if (timedOut)
                Assert.Fail("Method timed out");

            timedOut = false;
            clientBridge.MethodTimeout = timeout;
            // Check if the default timeout method times out when we lower the default timeout
            try
            {
                clientBridge.RemoteInterface.TimeoutDefault(sleeptime);
            }
            catch (TimeoutException)
            {
                timedOut = true;
            }
            if (!timedOut)
                Assert.Fail("Method did not timeout as specified by the default timeout property");
            
            clientBridge.Disconnect();
            serverBridge.Disconnect();
        }
        
        [Test]
        [ExpectedException(typeof(DisconnectedException))]
        public void DisconnectionExceptionTest()
        {
            // Setup
            IInterfacingBridge<IConnectionTestServer> clientBridge;
            IInterfacingBridge serverBridge;
            SetupServerConnection(Port, out clientBridge, out serverBridge);
            Server.StopServer();

            // Disconnect server
            serverBridge.Disconnect();

            // Send on client
            clientBridge.RemoteInterface.IsAlive();
        }

        [Test]
        public void DisconnectionCallbackTest()
        {
            // Setup
            IInterfacingBridge<IConnectionTestServer> clientBridge;
            IInterfacingBridge serverBridge;
            SetupServerConnection(Port, out clientBridge, out serverBridge);
            Server.StopServer();

            var taskCompletionSource = new TaskCompletionSource<bool>();
            clientBridge.Disconnected += (o, a) => taskCompletionSource.SetResult(true);

            // Disconnect server
            serverBridge.Disconnect();

            if (!taskCompletionSource.Task.Wait(100))
                Assert.Fail("Disconnection callback was not called within expected timespan");
            
        }
    }
}
