﻿using IRPC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IRPC.TCP.Test.ConnectionTest
{
    public class ConnectionTestObject : IConnectionTestServer, IConnectionTestClient
    {
        private AutoResetEvent _connectionReceivedEvent;
        
        public ConnectionTestObject()
        {

        }

        public ConnectionTestObject(AutoResetEvent connectionReceivedEvent)
        {
            _connectionReceivedEvent = connectionReceivedEvent;
        }

        public void OnConnectionReceived(IInterfacingBridge<IConnectionTestClient> bridge)
        {
            _connectionReceivedEvent.Set();
            bridge.Disconnect();
        }

        public void OnConnectionReceived(IInterfacingBridge<IConnectionTestServer> bridge)
        {
            _connectionReceivedEvent.Set();
            bridge.Disconnect();
        }
        
        public void Timeout500(int sleepTime)
        {
            Thread.Sleep(sleepTime);
        }

        public void Timeout1000(int sleepTime)
        {
            Thread.Sleep(sleepTime);
        }

        public void TimeoutDefault(int sleepTime)
        {
            Thread.Sleep(sleepTime);
        }

        public void IsAlive()
        {
        }        
    }
}
