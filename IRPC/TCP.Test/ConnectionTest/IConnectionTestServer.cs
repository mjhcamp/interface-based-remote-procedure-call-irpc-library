﻿using IRPC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRPC.TCP.Test.ConnectionTest
{
    public interface IConnectionTestServer
    {
        [RemotingTimeout(500)]
        void Timeout500(int sleepTime);

        [RemotingTimeout(1000)]
        void Timeout1000(int sleepTime);

        void TimeoutDefault(int sleepTime);

        void IsAlive();
    }
}
