﻿using System;
using System.Threading.Tasks;
namespace IRPC.TCP.Test
{
    public abstract class TCPTestBase<TServerInterface, TImpl>
        where TServerInterface : class
        where TImpl : class, TServerInterface, new()
    {
        protected TCPInterfacingServer Server { get; private set; }
        protected TCPInterfacingClient Client { get; private set; }

        public virtual void Initialize()
        {
            Server = new TCPInterfacingServer();
            Client = new TCPInterfacingClient();
        }

        protected TImpl SetupServerConnection(int port, out IInterfacingBridge<TServerInterface> clientBridge, out IInterfacingBridge serverBridge)
        {
            // Register interface
            var serverTaskCompletionSource = new TaskCompletionSource<IInterfacingBridge>();
            var server = new TImpl();
            Server.RegisterInterface<TServerInterface>((TServerInterface)server, serverTaskCompletionSource.SetResult);

            var clientTaskCompletionSource = new TaskCompletionSource<IInterfacingBridge<TServerInterface>>();
            var client = new TImpl();
            Client.RegisterInterface<TServerInterface>(clientTaskCompletionSource.SetResult);

            // Start both
            Server.StartServer(new UriBuilder("net.tcp", "127.0.0.1", port).Uri);
            Client.Connect(new UriBuilder("net.tcp", "127.0.0.1", port).Uri);

            clientTaskCompletionSource.Task.Wait();
            clientBridge = clientTaskCompletionSource.Task.Result;
            serverTaskCompletionSource.Task.Wait();
            serverBridge = serverTaskCompletionSource.Task.Result;

            Server.StopServer();

            return server;
        }

    }
}
