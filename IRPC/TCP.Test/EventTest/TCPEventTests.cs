﻿using NUnit.Framework;
using System;
using System.Threading.Tasks;
namespace IRPC.TCP.Test.EventTest
{
    public class TCPEventTests : TCPTestBase<IEventTestServer, EventTestObject>
    {
        private const int Port = 1338;
        private TaskCompletionSource<bool> _eventOccurredSource;

        [SetUp]
        public override void Initialize()
        {
            base.Initialize();
        }

        [Test]
        public void EventHandlerTest()
        {
            IInterfacingBridge<IEventTestServer> clientBridge;
            IInterfacingBridge serverBridge;

            var serverObject = SetupServerConnection(Port, out clientBridge, out serverBridge);
            _eventOccurredSource = new TaskCompletionSource<bool>();

            // Register to the event on the client side
            clientBridge.RemoteInterface.EventOccurred += RemoteInterfaceEventOccurred;

            // Invoke the event on the server side
            serverObject.InvokeMethod(new EventOccurredEventArgs());
         
            // Wait for the event to happen on the client side
            if (!_eventOccurredSource.Task.Wait(200))
                Assert.Fail("Event did not fire on client side");

            // Shutdown
            clientBridge.Disconnect();
            serverBridge.Disconnect();
            Server.StopServer();            
        }
        
        [Test]
        public void CustomDelegateEventTest()
        {
            IInterfacingBridge<IEventTestServer> clientBridge;
            IInterfacingBridge serverBridge;

            var serverObject = SetupServerConnection(Port, out clientBridge, out serverBridge);
            _eventOccurredSource = new TaskCompletionSource<bool>();

            // Register to the event on the client side
            clientBridge.RemoteInterface.TestDelegateEventOccurred += RemoteInterfaceTestDelegateEventOccurred;

            // Invoke the event on the server side
            var a = 3;
            var b = 5;
            var result = serverObject.InvokeTestDelegate(a, b);

            // Wait for the event to happen on the client side
            if (!_eventOccurredSource.Task.Wait(200))
                Assert.Fail("Event did not fire on client side");

            Assert.AreEqual(DoOperation(a,b), result);

            // Shutdown
            clientBridge.Disconnect();
            serverBridge.Disconnect();
            Server.StopServer();  
        }

        private int RemoteInterfaceTestDelegateEventOccurred(int a, int b)
        {
            _eventOccurredSource.SetResult(true);
            return DoOperation(a, b);
        }

        private int DoOperation(int a, int b)
        {
            return a + b;
        }

        private void RemoteInterfaceEventOccurred(object sender, EventOccurredEventArgs e)
        {
            _eventOccurredSource.SetResult(true);
        }
    }
}
