﻿using System;

namespace IRPC.TCP.Test.EventTest
{
    public class EventTestObject : IEventTestServer
    {
        public EventTestObject()
        {
        }

        public void InvokeMethod(EventOccurredEventArgs args)
        {
            var handler = EventOccurred;
            if (handler == null)
                return;

            handler(this, args);
        }

        public int InvokeTestDelegate(int a, int b)
        {
            var handler = TestDelegateEventOccurred;
            if (handler == null)
                return 0;

            return handler(a,b);
        }

        public event EventHandler<EventOccurredEventArgs> EventOccurred;
        
        public event TestDelegate TestDelegateEventOccurred;
    }
}
