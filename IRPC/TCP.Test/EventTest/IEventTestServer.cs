﻿using System;

namespace IRPC.TCP.Test.EventTest
{
    public delegate int TestDelegate(int a,int b);

    public interface IEventTestServer
    {
        event EventHandler<EventOccurredEventArgs> EventOccurred;

        event TestDelegate TestDelegateEventOccurred;
    }
}
