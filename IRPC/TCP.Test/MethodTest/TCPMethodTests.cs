﻿using NUnit.Framework;
using System;
namespace IRPC.TCP.Test.MethodTest
{
    [TestFixture]
    public class TCPMethodTests : TCPTestBase<IMethodTestServer, MethodTestObject>
    {
        private const int Port = 1339;

        private IInterfacingBridge<IMethodTestServer> _clientBridge;
        private IInterfacingBridge _serverBridge;
        private MethodTestObject _serverObject;

        [SetUp]
        public override void Initialize()
        {
            base.Initialize();
            _serverObject = SetupServerConnection(Port, out _clientBridge, out _serverBridge);
        }

        [TearDown]
        public void TearDown()
        {
            Server.StopServer();
            _clientBridge.Disconnect();
            _serverBridge.Disconnect();
        }

        [Test]
        public void ValueTypeMethodTest([Range(-100, 100, 50)]int a, [Range(-100, 100, 50)]int b)
        {          
            // Perform add operation over bridge
            var remoteResult = _clientBridge.RemoteInterface.Add(a, b);
            // Perform add operation directly on object
            var localResult = _serverObject.Add(a, b);

            Assert.AreEqual(remoteResult, localResult);
        }

        [Test]
        public void ArrayTypeMethodTest([Range(10,100,10)]int arraySize)
        {
            var random = new Random(DateTime.UtcNow.Millisecond);
            var a = new int[arraySize];
            var b = new int[arraySize];
            for(var i = 0;i<arraySize;i++)
            {
                a[i] = random.Next();
                b[i] = random.Next();
            }

            // Perform add operation over bridge
            var remoteResult = _clientBridge.RemoteInterface.Add(a, b);
            // Perform add operation directly on object
            var localResult = _serverObject.Add(a, b);

            Assert.AreEqual(remoteResult.Length, localResult.Length);

            for (var i = 0; i < arraySize; i++)
            {
                Assert.AreEqual(remoteResult[i], localResult[i]);
            }
        }

        [Test]
        public void ClassTypeMethodTest()
        {
            const string a = "Hello ";
            const string b = "world";
            
            // Perform add operation over bridge
            var remoteResult = _clientBridge.RemoteInterface.Concat(a, b);
            // Perform add operation directly on object
            var localResult = _serverObject.Concat(a, b);

            Assert.AreEqual(remoteResult, localResult);                        
        }

    }
}

