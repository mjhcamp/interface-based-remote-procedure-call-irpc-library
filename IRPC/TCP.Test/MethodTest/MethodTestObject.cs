﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRPC.TCP.Test.MethodTest
{
    public class MethodTestObject : IMethodTestServer
    {
        public int Add(int a, int b)
        {
            return a + b;
        }

        public int[] Add(int[] a, int[] b)
        {
            return a.Zip(b, (x, y) => x + y).ToArray();
        }

        public string Concat(string a, string b)
        {
            return a + b;
        }
    }
}
