﻿namespace IRPC.TCP.Test.MethodTest
{
    public interface IMethodTestServer
    {
        int Add(int a, int b);

        int[] Add(int[] a, int[] b);

        string Concat(string a, string b);
    }
}
