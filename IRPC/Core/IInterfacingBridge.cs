﻿using System;

namespace IRPC
{
    public interface IInterfacingBridge
    {
        Uri ConnectionUri { get; }
        
        void Disconnect();

        event EventHandler<DisconnectedEventArgs> Disconnected;
    }

    public interface IInterfacingBridge<out TRemote> : IInterfacingBridge
        where TRemote : class
    {
        TRemote RemoteInterface { get; }

        int MethodTimeout { get; set; }
    }
}
