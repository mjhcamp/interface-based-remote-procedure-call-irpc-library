﻿using System;
namespace IRPC
{
    public interface IInterfacingSerializer<TType>
    {
        ArraySegment<byte> Serialize(TType value);

        TType Deserialize(ArraySegment<byte> data);
    }
}
