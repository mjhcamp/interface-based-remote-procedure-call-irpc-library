﻿using System;

namespace IRPC
{
    public class DisconnectedEventArgs
    {
        public Exception Exception { get; set; }
    }
}
