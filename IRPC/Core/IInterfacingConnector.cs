﻿using System;

namespace IRPC
{
    public interface IInterfacingConnector
    {
        /// <summary>
        /// Register an interface for remote usage for one way communication
        /// </summary>
        /// <typeparam name="TLocal">Type of the local interface, must be an interface type and must be public</typeparam>
        /// <param name="localInterface">Implementation of the local interface</param>
        /// <param name="onConnectionEstablished">Callback for when a client has connected to this interface</param>
        void RegisterInterface<TLocal>(TLocal localInterface, Action<IInterfacingBridge> onConnectionEstablished)
            where TLocal : class;

        /// <summary>
        /// Register a callback function for when a remote interface of type TRemote connects
        /// </summary>
        /// <typeparam name="TRemote">Type of the remote interface</typeparam>
        /// <param name="onConnectionEstablished">Callback for when the desired remote interface has been connected</param>
        void RegisterInterface<TRemote>(Action<IInterfacingBridge<TRemote>> onConnectionEstablished)
            where TRemote : class;

        /// <summary>
        /// Register an IIinterfacingSerializer to use for the given type TType
        /// (note: The serializer will only be used if both ends have access to the/an assembly containing the serializer)
        /// </summary>
        /// <typeparam name="TType">Type for which the serializer is designed</typeparam>
        /// <param name="serializer">Serializer object to use for serializing objects of type TType</param>
        void RegisterSerializer<TType,TSerializer>()
            where TSerializer : IInterfacingSerializer<TType>, new()
            where TType : class;
    }
}
