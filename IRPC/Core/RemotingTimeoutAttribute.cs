﻿using System;

namespace IRPC
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Event, AllowMultiple = false)]
    public class RemotingTimeoutAttribute : Attribute
    {
        /// <summary>
        /// Create a new remoting timeout attribute specificying the timeout for a certain method
        /// </summary>
        /// <param name="timeout">Timeout in milliseconds</param>
        public RemotingTimeoutAttribute(ulong timeout)
        {
            Timout = timeout;
        }

        /// <summary>
        /// Timeout in milliseconds
        /// </summary>
        public ulong Timout { get; private set; }
    }
}
