﻿using System;

namespace IRPC
{
    [Serializable]
    public class RemoteException : Exception
    {
        public RemoteException(Exception e)
        {
            Exception = e;
        }

        public Exception Exception { get; private set; }
    }
}
