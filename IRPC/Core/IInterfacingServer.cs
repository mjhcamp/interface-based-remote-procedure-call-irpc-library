﻿using System;

namespace IRPC
{
    public interface IInterfacingServer : IInterfacingConnector     
    {
        void StartServer(Uri hostUri);

        void StopServer();
    }
}
