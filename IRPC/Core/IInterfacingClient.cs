﻿using System;

namespace IRPC
{
    public interface IInterfacingClient : IInterfacingConnector
    {
        void Connect(Uri connectionUri);
    }
}
