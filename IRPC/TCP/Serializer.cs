using System;

namespace IRPC.TCP
{
    internal class Serializer
    {
        internal Type SerializerType { get; set; }

        internal Type SerializedType { get; set; }

        internal Func<object, ArraySegment<byte>> SerializeFunction { get; set; }

        internal Func<ArraySegment<byte>, object> DeserializeFunction { get; set; } 
    }
}