﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Threading.Tasks;
using System.Reflection.Emit;
using IRPC.TCP.Messages;
using IRPC.TCP.Extensions;

namespace IRPC.TCP
{
    public class TCPInterfacingBridgeReceiver<TLocal> : TCPInterfacingBridgeBase
        where TLocal : class
    {
        #region Fields
        
        private readonly TLocal _localImplementation;
        private readonly object _eventListener;
        
        #endregion

        #region Constructors

        internal TCPInterfacingBridgeReceiver(Socket socket, TLocal localImplementation, IList<Serializer> serializers)
            : base(socket, serializers)
        {
            _localImplementation = localImplementation;

            _eventListener = InitialiseEvents();
            Initialise();
        }

        private object InitialiseEvents()
        {
            var clientId = Guid.NewGuid();
            var typeName = clientId.ToString().Replace("-", "");

            // Create a new assembly in memory
            var domain = AppDomain.CurrentDomain;
            var assemblyName = new AssemblyName("RemoteInterface.Server" + typeName);
#if DEBUG
            var clientAssembly = domain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.RunAndSave);
            var module = clientAssembly.DefineDynamicModule(assemblyName.Name, "EventListener.dll");     
#else
            var clientAssembly = domain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
            var module = clientAssembly.DefineDynamicModule(assemblyName.Name);     
#endif
            var newType = module.DefineType("C" + typeName, TypeAttributes.Public);

            var bridgeType = typeof(TCPInterfacingBridgeReceiver<TLocal>);
            var fields = newType.GenerateConstructor(bridgeType);
            var sendMethod = bridgeType.GetMethod("InvokeEventOnRemoteInterface", new []{ typeof(MethodInfo), typeof(object[])});
                        
            // Create methods that channel the event call to the InvokeEventOnRemoteInterface function
            foreach(var e in typeof(TLocal).GetEvents())
            {
                var delegateType = e.EventHandlerType;

                var method = newType.GenerateMethod(
                    MethodAttributes.Public, 
                    fields[0], 
                    sendMethod,
                    "invoke_" + e.Name,
                    delegateType.GetDelegateParameterTypes(),
                    delegateType.GetDelegateReturnType());
            }
            
            // Create the type
            var t = newType.CreateType();
#if DEBUG
            clientAssembly.Save("EventListener.Debug.dll");
#endif
            var obj = Activator.CreateInstance(t, this);

            // Subscribe all methods to the correct events
            foreach (var e in typeof(TLocal).GetEvents())
            {
                var delegateType = e.EventHandlerType;
                var invokeMethod = delegateType.GetMethod("Invoke");

                invokeMethod = t.GetMethod(
                    "invoke_" + e.Name,
                    delegateType.GetDelegateParameterTypes());

                var methodDelegate = Delegate.CreateDelegate(delegateType, obj, invokeMethod);
                e.AddEventHandler(_localImplementation, methodDelegate);
            }

            return obj;
        }
        
        #endregion

        #region Transmitter/Receiver

        internal override void OnMethodCallReceived(MethodCall methodCall)
        {
            Task.Factory.StartNew(o =>
            {
                var mc = (MethodCall)o;

                // Find the method
                var localType = typeof(TLocal);
                var interfaces = localType.GetInterfaces().ToList();
                interfaces.Insert(0, localType);

                var argumentTypes = mc.MethodArguments.Select(x => x.GetType()).ToArray();
                var method = interfaces.Select(x => x.GetMethod(mc.MethodName, argumentTypes)).FirstOrDefault(y => y != null);

                Exception exception = null;
                object returnValue = null;
                try
                {
                    // Invoke method 
                    returnValue = method.Invoke(_localImplementation, mc.MethodArguments);
                }
                catch (Exception e)
                {
                    exception = e;
                }
                SendMethodReturn(new MethodReturn(mc.MethodCallId, returnValue, exception));

            }, methodCall);
        }
               
        #endregion

        #region Method Invocation
      
        public object InvokeEventOnRemoteInterface(MethodInfo method, object[] arguments)
        {
            if (!method.Name.StartsWith("invoke_"))
                throw new InvalidOperationException("Only event invocation methods are supported");

            // Check if the method invokes an event with an for a System.EventHandler
            var eventName = method.Name.Substring(7);
            var e = typeof(TLocal).GetEvent(eventName);

            var args = arguments;
            if (e.EventHandlerType == typeof(EventHandler) ||
                (e.EventHandlerType.IsGenericType &&
                e.EventHandlerType.GetGenericTypeDefinition() == typeof(EventHandler<>)))
            {
                // Remove the first argument (we cannot send the sending object)
                args = arguments.Skip(1).ToArray();
            }

            // Queue methodcall
            var call = new MethodCall(method.Name, args);
            var returnValue = SendMethodCall(call, TimeSpan.FromSeconds(5));

            if (returnValue.MethodException != null)
            {
                throw new RemoteException(returnValue.MethodException);
            }

            return returnValue.MethodReturnValue;
        }

        #endregion        
    }
}
