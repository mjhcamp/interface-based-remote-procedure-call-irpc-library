﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using IRPC.TCP.Messages;
using System.Net;

namespace IRPC.TCP
{
    public abstract class TCPInterfacingBridgeBase : IInterfacingBridge
    {
        #region Constants and Defaults

        protected const int PollingTime = 50;
        protected const int ReceiveBufferSize = 1 << 24;
        protected const int CommunicationTimeout = 10000;
        protected const int MethodTimeoutDefault = 2000;

        #endregion

        #region Fields
        private readonly IList<MethodCall> _pendingMethodCalls;
        private readonly IList<MethodReturn> _pendingMethodReturns;

        private readonly IList<MethodReturn> _receivedMethodReturns;
        private readonly IList<MethodCall> _methodsPendingForReturn;

        private readonly AutoResetEvent _sendOperationQueued;

        private readonly IList<Serializer> _serializers;
        private readonly object _disconnectLock;

        private readonly CancellationTokenSource _workerThreadTokenSource;
        private readonly Thread _receiverThread;
        private readonly Thread _transmitterThread;
        private Socket _socket;
        #endregion

        #region Properties

        protected bool IsDisconnected
        {
            get;
            private set;
        }

        #endregion
        
        #region IInterfacingBridge Members

        public Uri ConnectionUri
        {
            get;
            private set;
        }

        public void Disconnect()
        {
            lock (_disconnectLock)
            {
                // Check if we are already disconnected
                if (_socket == null)
                    return;

                // Signal the workers to stop
                _workerThreadTokenSource.Cancel();

                // Join worker threads
                _receiverThread.Join();
                _transmitterThread.Join();

                // Close bridge
                CloseBridge();
            }
        }

        public event EventHandler<DisconnectedEventArgs> Disconnected;

        #endregion
        
        #region Constructors and Initialisation

        internal TCPInterfacingBridgeBase(Socket socket, IList<Serializer> serializers)
        {
            _socket = socket;
            _serializers = serializers;

            _socket.ReceiveTimeout = CommunicationTimeout;
            _socket.SendTimeout = CommunicationTimeout;

            _disconnectLock = new object();

            _pendingMethodCalls = new List<MethodCall>();
            _pendingMethodReturns = new List<MethodReturn>();

            _methodsPendingForReturn = new List<MethodCall>();
            _receivedMethodReturns = new List<MethodReturn>();

            _sendOperationQueued = new AutoResetEvent(false);

            _workerThreadTokenSource = new CancellationTokenSource();
            
            _receiverThread = new Thread(ReceiverWorker);
            _transmitterThread = new Thread(TransmitterWorker);

            var remoteEndPoint = (IPEndPoint)socket.RemoteEndPoint;
            var uriBuilder = new UriBuilder { Host = remoteEndPoint.Address.ToString(), Port = remoteEndPoint.Port, Scheme = "net.tcp" };
            ConnectionUri = uriBuilder.Uri;
        }

        protected void Initialise()
        {
            _receiverThread.Start(_workerThreadTokenSource.Token);
            _transmitterThread.Start(_workerThreadTokenSource.Token);
        }

        #endregion

        #region Child Implemented Functions

        internal abstract void OnMethodCallReceived(MethodCall methodCall);
        
        #endregion

        #region Child Accessible Functions

        internal MethodReturn SendMethodCall(MethodCall call, TimeSpan timeout)
        {
            _pendingMethodCalls.Add(call);

            if (IsDisconnected)
            {
                _pendingMethodCalls.Remove(call);
                throw new DisconnectedException();
            }

            _sendOperationQueued.Set();
                     
            // Wait for result
            if (!call.MethodCallReturned.WaitOne(timeout))
            {
                lock (_pendingMethodCalls)
                {
                    // Stop waiting for the method to return..
                    _pendingMethodCalls.Remove(call);
                }
                throw new TimeoutException("Remote interface method timed out on " + call.MethodName);
            }

            if (IsDisconnected)
            {
                _pendingMethodCalls.Remove(call);
                throw new DisconnectedException();
            }
            
            // Check if method has returned
            lock (_receivedMethodReturns)
            {
                var returnValue = _receivedMethodReturns.Single(x => x.MethodCallId == call.MethodCallId);
                _receivedMethodReturns.Remove(returnValue);
                return returnValue;
            }
        }

        internal void SendMethodReturn(MethodReturn methodReturn)
        {
            // Queue method return
            _pendingMethodReturns.Add(methodReturn);
            _sendOperationQueued.Set();
        }

        #endregion

        #region Sender/Receiver Methods
        
        private void TransmitterWorker(object cancelToken)
        {
            try
            {
                TransmitterLoop((CancellationToken)cancelToken);
            }
            catch (Exception e)
            {
                Disconnect(e, _receiverThread);
            }
        }

        private void ReceiverWorker(object cancelToken)
        {
            try
            {
                ReceiverLoop((CancellationToken)cancelToken);
            }
            catch (Exception e)
            {
                Disconnect(e is DisconnectedException ? null : e, _transmitterThread);
            }
        }
                
        private void TransmitterLoop(CancellationToken token)
        {
            var binSerializer = new BinaryFormatter();

            while (true)
            {
                while (!_sendOperationQueued.WaitOne(PollingTime))
                {
                    if (token.IsCancellationRequested)
                        return;
                }

                // Send all pending return values
                for (var i = 0; i < _pendingMethodReturns.Count; )
                {
                    var returnValue = _pendingMethodReturns[i];
                    using (var s = new MemoryStream())
                    {
                        s.Write(new byte[sizeof(int)], 0, sizeof(int));
                        s.WriteByte((byte)MessageType.Return);
                        binSerializer.Serialize(s, returnValue);
                        var byteBuf = s.ToArray();
                        Array.Copy(BitConverter.GetBytes(byteBuf.Length), byteBuf, sizeof(int));

                        _socket.Send(byteBuf);
                    }

                    _pendingMethodReturns.Remove(returnValue);
                }

                // Send all pending methods
                for (var i = 0; i < _pendingMethodCalls.Count; )
                {
                    var methodCall = _pendingMethodCalls[i];

                    // Remove method call from pending method call list and add to pending for return list
                    _pendingMethodCalls.Remove(methodCall);
                    lock (_methodsPendingForReturn)
                    {
                        _methodsPendingForReturn.Add(methodCall);
                    }

                    // Send payload
                    SendPacket(SerializeMethodCall(methodCall));
                }
            }          
        }

        private void ReceiverLoop(CancellationToken token)
        {
            var receiveBuffer = new byte[ReceiveBufferSize];

            while (true)
            {
                var packetLength = ReceivePacket(receiveBuffer, token);
                if (packetLength == 0)
                    return;

                switch ((MessageType)receiveBuffer[0])
                {
                    case MessageType.Call:
                        var methodCall = DeserializeMethodCall(receiveBuffer);
                        if (methodCall != null)
                            OnMethodCallReceived(methodCall);
                        break;

                    case MessageType.Return:
                        var methodReturn = DeserializeMethodReturn(receiveBuffer);
                        if (methodReturn != null)
                        {
                            lock (_receivedMethodReturns)
                            {
                                _receivedMethodReturns.Add(methodReturn);
                            }
                            lock (_methodsPendingForReturn)
                            {
                                var receivedReturn = _methodsPendingForReturn.SingleOrDefault(x => x.MethodCallId == methodReturn.MethodCallId);
                                if (receivedReturn == null)
                                    return;

                                _methodsPendingForReturn.Remove(receivedReturn);
                                receivedReturn.MethodCallReturned.Set();
                            }
                        }
                        break;
                }
            }
        }
        
        private void SendPacket(IList<ArraySegment<byte>> packet)
        {
            var packetlength = packet.Select(x => x.Count).Aggregate((x, y) => x + y) + sizeof(int);
            packet.Insert(0, new ArraySegment<byte>(BitConverter.GetBytes(packetlength)));

            _socket.Send(packet);
        }

        private int ReceivePacket(byte[] buffer, CancellationToken token)
        {
            while (true)
            {
                if (token.IsCancellationRequested)
                    return 0;

                if (_socket.Poll(PollingTime * 1000, SelectMode.SelectRead))
                    break;
            }

            var receivedLength = 0;
            var lengthBuffer = new byte[sizeof(int)];
            while (receivedLength < sizeof(int))
            {
                var received = _socket.Receive(lengthBuffer, receivedLength, sizeof(int) - receivedLength,
                    SocketFlags.None);

                if (received == 0)
                    throw new DisconnectedException();

                receivedLength += received;
            }

            var packetLength = BitConverter.ToInt32(lengthBuffer, 0) - sizeof(int);
            receivedLength = 0;
            while (receivedLength < packetLength)
            {
                var received = _socket.Receive(buffer, receivedLength, packetLength - receivedLength,
                    SocketFlags.None);

                if (received == 0)
                    throw new DisconnectedException();

                receivedLength += received;
            }

            return packetLength;
        }
        
        #endregion

        #region Disconnection Methods

        private void CloseBridge()
        {
            _socket.Close();
            _socket = null;

            IsDisconnected = true;
            foreach (var pendingMethod in _methodsPendingForReturn)
                pendingMethod.MethodCallReturned.Set();
        }

        private void Disconnect(Exception e, Thread joinThread)
        {
            // This lock makes sure that if this function is called by both worker threads, we wont get deadlocks
            if (!Monitor.TryEnter(_disconnectLock))
                return;
            
            // Signal the workers to stop if they have not already done so
            _workerThreadTokenSource.Cancel();

            // Join other thread
            joinThread.Join();

            // Close bridge
            CloseBridge();
            
            // Notify event listeners
            var handler = Disconnected;
            if (handler != null)
                handler(this, new DisconnectedEventArgs { Exception = e });

            // Free lock
            Monitor.Exit(_disconnectLock);
        }

        #endregion

        #region Serialization Methods

        internal IList<ArraySegment<byte>> SerializeMethodCall(MethodCall methodCall)
        {
            var replacedMethodCall = new MethodCall
            {
                MethodCallId = methodCall.MethodCallId,
                MethodName = methodCall.MethodName,
                MethodArguments = new object[methodCall.MethodArguments.Length]
            };

            var serializedArguments = new List<ArraySegment<byte>>();
            // Check if the methodcall contains any type that is customly serialized
            for(var i = 0;i<methodCall.MethodArguments.Length;i++)
            {
                var argument = methodCall.MethodArguments[i];
                var type = argument.GetType();
                var serializer = GetSerializerForType(type);

                if (serializer == null)
                {
                    replacedMethodCall.MethodArguments[i] = methodCall.MethodArguments[i];
                    continue;
                }

                var serialized = serializer.SerializeFunction(argument);
                var byteOffset = serializedArguments.Any() ? serializedArguments.Select(x => x.Count).Aggregate((x, y) => x + y) : 0;

                replacedMethodCall.MethodArguments[i] = new MethodArgumentSerializationToken
                {
                    ByteLength = serialized.Count,
                    ByteOffset = byteOffset,
                    SerializedTypeName = type.AssemblyQualifiedName
                };
                serializedArguments.Add(serialized);
            }
            
            // Serialize replaced methodcall and send
            var binSerializer = new BinaryFormatter();
            using (var memoryStream = new MemoryStream())
            {
                binSerializer.Serialize(memoryStream, replacedMethodCall);
                serializedArguments.Insert(0,new ArraySegment<byte>(memoryStream.ToArray()));
            }

            serializedArguments.Insert(0, new ArraySegment<byte>(new []{(byte)MessageType.Call}));            
            return serializedArguments;
        }

        internal IList<ArraySegment<byte>> SerializeMethodReturn(MethodReturn methodReturn)
        {
            var replacedReturn = new MethodReturn
            {
                MethodCallId = methodReturn.MethodCallId,
                MethodException = methodReturn.MethodException,
                MethodReturnValue = methodReturn.MethodReturnValue
            };

            var returnList = new List<ArraySegment<byte>>();
            var binSerializer = new BinaryFormatter();
            using (var s = new MemoryStream())
            {
                s.Write(new byte[sizeof(int)], 0, sizeof(int));
                s.WriteByte((byte)MessageType.Return);
                if (methodReturn.MethodException == null)
                {       
                    // Check if there is a serializer for the method return type
                    var returnType = methodReturn.MethodReturnValue.GetType();
                    var serializer = GetSerializerForType(returnType);
                    if (serializer != null)
                    {
                        var serialized = serializer.SerializeFunction(methodReturn.MethodReturnValue);
                        returnList.Add(serialized);

                        replacedReturn.MethodReturnValue = new MethodArgumentSerializationToken
                        {
                            ByteLength = serialized.Count,
                            ByteOffset = 0,
                            SerializedType = returnType
                        };
                    }
                }

                binSerializer.Serialize(s, replacedReturn);
                returnList.Insert(0, new ArraySegment<byte>(s.ToArray()));
                return returnList;
            }
        }

        internal MethodCall DeserializeMethodCall(byte[] bytes)
        {
            var messageType = (MessageType)bytes[0];
            if (messageType != MessageType.Call)
                throw new Exception("Invalid MethodCall");

            // Deserialize methodcall structure
            var binSerializer = new BinaryFormatter();
            MethodCall methodCall;
            int byteOffset;
            using (var memoryStream = new MemoryStream(bytes,1,bytes.Length-1))
            {              
                methodCall = binSerializer.Deserialize(memoryStream) as MethodCall;
                byteOffset = (int)memoryStream.Position + 1;
            }
            if(methodCall == null)
                throw new Exception("Invalid MethodCall");

            // Check all arguments for custom serialization
            for (var i = 0; i < methodCall.MethodArguments.Length; i++)
            {
                var argument = methodCall.MethodArguments[i];
                var type = argument.GetType();

                if (type != typeof (MethodArgumentSerializationToken))
                    continue;

                var token = (MethodArgumentSerializationToken) argument;

                // Get serializer
                var serializer = GetSerializerForType(token.SerializedType);
                if (serializer == null)
                    throw new InvalidOperationException("Unknown serializer for type " + token.SerializedType);

                var bufferSegment = new ArraySegment<byte>(bytes, byteOffset + token.ByteOffset, token.ByteLength);

                methodCall.MethodArguments[i] = serializer.DeserializeFunction(bufferSegment);
            }

            return methodCall;
        }

        internal MethodReturn DeserializeMethodReturn(byte[] bytes)
        {
            var messageType = (MessageType)bytes[0];
            if (messageType != MessageType.Return)
                throw new Exception("Invalid MethodReturn");

            var binSerializer = new BinaryFormatter();
            MethodReturn methodReturn;
            using (var s = new MemoryStream(bytes,1,bytes.Length-1))
            {
                methodReturn = binSerializer.Deserialize(s) as MethodReturn;
            }
            
            // Check for serializers
            if (!(methodReturn.MethodReturnValue is MethodArgumentSerializationToken))
                return methodReturn;

            var token = (MethodArgumentSerializationToken)methodReturn.MethodReturnValue;
            var serializer = GetSerializerForType(token.SerializedType);
            if (serializer == null)
                throw new InvalidOperationException("Unknown serializer for type " + token.SerializedType);

            methodReturn.MethodReturnValue = serializer.DeserializeFunction(new ArraySegment<byte>(bytes, token.ByteOffset, token.ByteLength));

            return methodReturn;
        }

        private Serializer GetSerializerForType(Type type)
        {
            return _serializers.FirstOrDefault(x => type == x.SerializedType) ??
                                _serializers.FirstOrDefault(x => x.SerializedType.IsAssignableFrom(type));
        }

        #endregion
             
    }
}
