﻿using System;
using System.Threading;

namespace IRPC.TCP.Messages
{
    [Serializable]
    internal class MethodCall
    {
        [NonSerialized]
        private readonly AutoResetEvent _methodCallReturned;

        internal MethodCall(string methodName, object[] arguments)
        {
            MethodName = methodName;
            MethodArguments = arguments;
            MethodCallId = Guid.NewGuid();
            _methodCallReturned = new AutoResetEvent(false);
        }

        internal MethodCall()
        {

        }

        internal AutoResetEvent MethodCallReturned { get { return _methodCallReturned; } }

        internal Guid MethodCallId { get; set; }

        internal string MethodName { get; set; }

        internal object[] MethodArguments { get; set; }
    }
}
