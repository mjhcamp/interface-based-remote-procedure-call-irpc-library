﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRPC.TCP.Messages
{
    [Serializable]
    internal class MethodReturn
    {
        internal MethodReturn(Guid methodId, object returnValue, Exception methodException)
        {
            MethodCallId = methodId;
            MethodReturnValue = returnValue;
            MethodException = methodException;
        }

        internal MethodReturn()
        {

        }

        internal Guid MethodCallId { get; set; }

        internal object MethodReturnValue { get; set; }

        internal Exception MethodException { get; set; }
    }
}
