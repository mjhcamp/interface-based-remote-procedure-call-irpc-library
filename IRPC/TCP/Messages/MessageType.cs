﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRPC.TCP.Messages
{
    internal enum MessageType : byte
    {
        Call = 1,
        Return
    }
}
