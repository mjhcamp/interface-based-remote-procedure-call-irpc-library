﻿using System;

namespace IRPC.TCP.Messages
{
    [Serializable]
    internal class MethodArgumentSerializationToken
    {
        internal string SerializedTypeName { get; set; }

        internal Type SerializedType 
        { 
            get 
            { 
                return Type.GetType(SerializedTypeName);
            } 
            set
            {
                SerializedTypeName = value.AssemblyQualifiedName;
            }
        }

        internal int ByteOffset { get; set; }

        internal int ByteLength { get; set; }
    }
}
