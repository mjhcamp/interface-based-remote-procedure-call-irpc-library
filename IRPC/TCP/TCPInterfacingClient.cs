﻿using System;
using System.Net;
using System.Net.Sockets;

namespace IRPC.TCP
{
    public class TCPInterfacingClient : TCPInterfacingConnectorBase, IInterfacingClient
    {
        private Socket _socket;

        public void Connect(Uri connectionUri)
        {
            // Parse URI
            if (connectionUri.Scheme != "net.tcp")
                throw new ArgumentException("Invalid URI protocol should be net.tcp");
            
            var endpoint = new IPEndPoint(IPAddress.Parse(connectionUri.Host), connectionUri.Port);

            try
            {
                // Create a socket and connect    
                _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                _socket.Bind(new IPEndPoint(IPAddress.Any, 0));

                _socket.Connect(endpoint);

                OnConnectionEstablished(_socket);
            }
            catch(Exception)
            {
                try
                {
                    if(_socket != null)
                        _socket.Close();
                } 
                catch(SocketException)
                {

                }
                throw;
            }
        }
    }
}
