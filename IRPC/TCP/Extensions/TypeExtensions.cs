﻿using System;
using System.Linq;
using System.ComponentModel;


namespace IRPC.TCP.Extensions
{
    public static class TypeExtensions
    {
        public static bool IsSerializable(this Type type)
        {
            var attributes = TypeDescriptor.GetAttributes(type);

            return attributes.OfType<SerializableAttribute>().Any();
        }

        public static Type[] GetDelegateParameterTypes(this Type d)
        {
            if (d.BaseType != typeof(MulticastDelegate))
                throw new ApplicationException("Not a delegate.");

            var invoke = d.GetMethod("Invoke");
            if (invoke == null)
                throw new ApplicationException("Not a delegate.");

            var parameters = invoke.GetParameters();
            var typeParameters = new Type[parameters.Length];
            for (int i = 0; i < parameters.Length; i++)
            {
                typeParameters[i] = parameters[i].ParameterType;
            }
            return typeParameters;
        }


        public static Type GetDelegateReturnType(this Type d)
        {
            if (d.BaseType != typeof(MulticastDelegate))
                throw new ApplicationException("Not a delegate.");

            var invoke = d.GetMethod("Invoke");
            if (invoke == null)
                throw new ApplicationException("Not a delegate.");

            return invoke.ReturnType;
        }
    }
}
