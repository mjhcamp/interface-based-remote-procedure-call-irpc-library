﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace IRPC.TCP.Extensions
{
    public static class TypeBuilderExtensions
    {
        /// <summary>
        /// Generates a constructor for a given class that takes <paramref name="arguments"/> as input and assigns them as fields in the class
        /// </summary>
        /// <param name="arguments"></param>
        public static FieldBuilder[] GenerateConstructor(
            this TypeBuilder builder, 
            params Type[] arguments)
        {
            // Define fields foreach of the input params
            var fields = new FieldBuilder[arguments.Length];

            var constructorBuilder = builder.DefineConstructor(
                MethodAttributes.Public |
                MethodAttributes.HideBySig |
                MethodAttributes.SpecialName |
                MethodAttributes.RTSpecialName, 
                CallingConventions.HasThis, arguments);
            var ilGen = constructorBuilder.GetILGenerator();
            
            var objectCtor = typeof(object).GetConstructor(new Type[0]);

            // Call object constructor
            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Call, objectCtor);

            for (var i = 0; i < arguments.Length;i++ )
            {
                fields[i] = builder.DefineField("_arg" + i, arguments[i], FieldAttributes.Private | FieldAttributes.InitOnly);
                ilGen.Emit(OpCodes.Ldarg_0);
                ilGen.Emit(OpCodes.Ldarg, (short)(i + 1));
                ilGen.Emit(OpCodes.Stfld, fields[i]);
            }

            ilGen.Emit(OpCodes.Ret);

            return fields;
        }

        public static MethodBuilder GenerateMethod(
            this TypeBuilder builder, 
            MethodAttributes attributes,
            FieldBuilder callingObject, 
            MethodInfo callingMethod, 
            string methodName, 
            Type[] arguments,
            Type returnType)
        {
            var methodBuilder = builder.DefineMethod(
                         methodName,
                         attributes,
                         returnType,
                         arguments);

            var languageGenerator = methodBuilder.GetILGenerator();
            var currentMethodMethod = typeof(MethodBase).GetMethod("GetCurrentMethod");

            var methodField = languageGenerator.DeclareLocal(typeof(MethodInfo));
            var arrayField = languageGenerator.DeclareLocal(typeof(object[]));

            /// C#: MethodBase.GetCurrentMethod()
            // Get the method 
            languageGenerator.Emit(OpCodes.Call, currentMethodMethod);
            // Store method name as local variable
            languageGenerator.Emit(OpCodes.Stloc, methodField);

            /// C#: var arguments = new object[arguments.Length];
            // Push the number of elements in the object array to the stack
            languageGenerator.Emit(OpCodes.Ldc_I4, arguments.Length);
            // Create an array (pushing the array to the stack)
            languageGenerator.Emit(OpCodes.Newarr, typeof(object));
            // Store array as local variable
            languageGenerator.Emit(OpCodes.Stloc, arrayField);

            for (ushort i = 0; i < arguments.Length; i++)
            {
                // Push array to stack
                languageGenerator.Emit(OpCodes.Ldloc, arrayField);

                /// C#: arguments[i] = varargs[i];
                // Push index to the stack
                languageGenerator.Emit(OpCodes.Ldc_I4, i);

                // Push value to the stack
                languageGenerator.Emit(OpCodes.Ldarg, i + 1);

                if (arguments[i].IsValueType)
                {
                    // Box value
                    languageGenerator.Emit(OpCodes.Box, arguments[i]);
                }

                // Assing to array
                languageGenerator.Emit(OpCodes.Stelem_Ref);
            }

            /// C#: _bridgeInterface.InvokeMethodOnRemoteInterface(method, arguments);
            // push object instance to stack
            languageGenerator.Emit(OpCodes.Ldarg_0);
            // Push socket to stack            
            languageGenerator.Emit(OpCodes.Ldfld, callingObject);

            // Push method to stack
            languageGenerator.Emit(OpCodes.Ldloc, methodField);

            // Push array to stack
            languageGenerator.Emit(OpCodes.Ldloc, arrayField);

            // Call send method
            languageGenerator.Emit(OpCodes.Call, callingMethod);

            // If there is no return value, pop the object from the stack
            if (returnType == null || returnType == typeof(void))
                languageGenerator.Emit(OpCodes.Pop);

            else if (returnType != null && returnType.IsValueType)
                languageGenerator.Emit(OpCodes.Unbox_Any, returnType);

            // C#: return
            languageGenerator.Emit(OpCodes.Ret);

            return methodBuilder;
        }
    }
}
