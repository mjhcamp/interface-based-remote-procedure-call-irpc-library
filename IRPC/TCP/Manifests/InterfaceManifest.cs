﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace IRPC.TCP.Manifests
{
    [DataContract]
    internal class InterfaceManifest
    {
        [DataMember]
        internal List<string> InterfaceTypeNames
        {
            get;
            set;
        }

        internal IEnumerable<Type> InterfaceTypes
        {
            get {
                return InterfaceTypeNames.Select(Type.GetType);
            }
        }
    }
}
