﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace IRPC.TCP.Manifests
{

    [DataContract]
    internal class SerializerManifest
    {
        [DataMember]
        internal List<String> SerializerTypeNames { get; set; }

        internal IEnumerable<Type> SerializerTypes
        {
            get
            {
                return SerializerTypeNames.Select(Type.GetType);
            }
        } 
    }
}
