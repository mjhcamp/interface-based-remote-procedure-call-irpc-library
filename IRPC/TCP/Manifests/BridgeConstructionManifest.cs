﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IRPC.TCP.Manifests
{
    [DataContract]
    internal class BridgeConstruct
    {
        [DataMember]
        internal string InterfaceName { get; set; }

        [DataMember]
        internal int ReceiverPort { get; set; }

        internal Type InterfaceType { get { return Type.GetType(InterfaceName); } }
    }

    [DataContract]
    internal class BridgeConstructionManifest
    {
        [DataMember]
        internal List<BridgeConstruct> BridgeConstructs { get; set; }
    }
}
