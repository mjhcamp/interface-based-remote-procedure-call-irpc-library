﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using IRPC.TCP.Messages;
using IRPC.TCP.Extensions;

namespace IRPC.TCP
{
    public class TCPInterfacingBridge<TRemote> : TCPInterfacingBridgeBase, IInterfacingBridge<TRemote>
        where TRemote : class
    {
        #region Fields

        private readonly IList<KeyValuePair<MethodInfo, ulong>> _methodTimeouts; 
        
        #endregion
        
        #region IInterfacingBridge<TRemote> members

        public int MethodTimeout
        {
            get;
            set;
        }

        public TRemote RemoteInterface
        {
            get;
            private set;
        }

        #endregion

        #region Constructor and Initialization

        internal TCPInterfacingBridge(Socket socket, IList<Serializer> serializers)
            : base(socket, serializers)
        {
            _methodTimeouts = new List<KeyValuePair<MethodInfo, ulong>>();
            
            RemoteInterface = InitializeRemoteInterface();
            MethodTimeout = MethodTimeoutDefault;
            Initialise();
        }

        private TRemote InitializeRemoteInterface()
        {
            // Create a dynamic assembly for this type
            var clientId = Guid.NewGuid();
            var domain = AppDomain.CurrentDomain;
            var assemblyName = new AssemblyName("RemoteInterface.Client" + clientId.ToString().Replace("-", ""));

            // Create a module for executing code
#if DEBUG
            var clientAssembly = domain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.RunAndSave);
            var module = clientAssembly.DefineDynamicModule(assemblyName.Name, "RemoteInterface.dll");
#else
            var clientAssembly = domain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
            var module = clientAssembly.DefineDynamicModule(assemblyName.Name);
#endif

            // Create stub interfaces for remote interface
            var interfaceType = typeof(TRemote);
            var typeName = interfaceType.Name;
            if (typeName[0] == 'I')
                typeName = typeName.Substring(1);

            // Create a type that implements this interface
            var implementingType = module.DefineType(typeName, TypeAttributes.Public);
            implementingType.AddInterfaceImplementation(interfaceType);

            // Give the new type a constructor taking the connection socket
            var fields = implementingType.GenerateConstructor(new[] { typeof(TCPInterfacingBridge<TRemote>) });
            FieldBuilder bridgeField = fields[0];

            var propertyGetterSetters = new List<MethodBuilder>();
            // Find all methods in type and base types
            var interfaces = interfaceType.GetInterfaces().ToList();
            interfaces.Insert(0,interfaceType);

            var sendMethodInfo = GetType().GetMethod("InvokeMethodOnRemoteInterface",new []{typeof(MethodInfo),typeof(object[])});

            // Define all methods
            foreach (var method in interfaces.SelectMany(x=>x.GetMethods()))
            {
                if (method.Name.StartsWith("add_") || method.Name.StartsWith("remove_"))
                    continue;

                var builder = implementingType.GenerateMethod(
                    MethodAttributes.Public | MethodAttributes.Virtual,
                    bridgeField, 
                    sendMethodInfo, 
                    method.Name, 
                    method.GetParameters().Select(x=>x.ParameterType).ToArray(),
                    method.ReturnType);

                if (method.Name.StartsWith("set_") || method.Name.StartsWith("get_"))
                    propertyGetterSetters.Add(builder);
                else
                {
                    var attribute = method.GetCustomAttribute(typeof(RemotingTimeoutAttribute)) as RemotingTimeoutAttribute;
                    if (attribute == null)
                        continue;

                    _methodTimeouts.Add(new KeyValuePair<MethodInfo, ulong>(method, attribute.Timout));
                }
            }
            
            // Define all properties
            foreach (var property in interfaces.SelectMany(x => x.GetProperties()))
            {
                // Take the first getter and first setter in case of the New keyword
                var setter = propertyGetterSetters.FirstOrDefault(x => x.Name == "set_" + property.Name);
                var getter = propertyGetterSetters.FirstOrDefault(x => x.Name == "get_" + property.Name);

                BuildPropertyForPropertyInfo(
                    implementingType, 
                    property,
                    setter,
                    getter);

                var attribute = property.GetCustomAttribute(typeof(RemotingTimeoutAttribute)) as RemotingTimeoutAttribute;
                if (attribute == null)
                    continue;
                
                _methodTimeouts.Add(new KeyValuePair<MethodInfo, ulong>(setter, attribute.Timout));
                _methodTimeouts.Add(new KeyValuePair<MethodInfo, ulong>(getter, attribute.Timout));
            }

            // Define all events
            foreach (var e in interfaces.SelectMany(x => x.GetEvents()))
            {
                BuildEventForEventInfo(implementingType, e);
            }

            var type = implementingType.CreateType();

#if DEBUG
            clientAssembly.Save("RemoteInterface.Debug.dll");
#endif

            // Instantiate
            return Activator.CreateInstance(type, this) as TRemote;
        }
              
        #endregion
        
        #region Transmitter/receiver
        
        internal override void OnMethodCallReceived(MethodCall methodCall)
        {
            Task.Factory.StartNew(o =>
            {
                var mc = (MethodCall)o;

                // Find the method
                var localType = RemoteInterface.GetType();
                var method = localType.GetMethod(mc.MethodName, mc.MethodArguments.Select(x=>x.GetType()).ToArray());

                Exception exception = null;
                object returnValue = null;
                try
                {
                    // Invoke method 
                    returnValue = method.Invoke(RemoteInterface, mc.MethodArguments);
                }
                catch (Exception e)
                {
                    exception = e;
                }
                // Queue method return
                SendMethodReturn(new MethodReturn(mc.MethodCallId, returnValue, exception));

            }, methodCall);
        }

        #endregion
        
        #region Method invocation

        public object InvokeMethodOnRemoteInterface(MethodInfo method, object[] arguments)
        {
            // Queue methodcall
            var call = new MethodCall(method.Name, arguments);
            var callId = call.MethodCallId;

            var argumentTypes = arguments.Select(x => x.GetType());

            var timeout = MethodTimeout;
            if (_methodTimeouts.Any(x => x.Key.Name == method.Name &&
                         x.Key.GetParameters().Select(y => y.ParameterType).SequenceEqual(argumentTypes)))
            {
                timeout = (int)_methodTimeouts.Single(x => x.Key.Name == method.Name &&
                                                      x.Key.GetParameters()
                                                          .Select(y => y.ParameterType)
                                                          .SequenceEqual(argumentTypes)).Value;
            }

            var returnValue = SendMethodCall(call, TimeSpan.FromMilliseconds(timeout));            
            if (returnValue.MethodException != null)
            {
                throw new RemoteException(returnValue.MethodException);
            }

            return returnValue.MethodReturnValue;        
        }

        #endregion

        #region MSIL Builder Helpers
        // These functions create interface implementations at runtime

        private static void BuildPropertyForPropertyInfo(TypeBuilder builder, PropertyInfo propertyInfo, MethodBuilder setterBuilder, MethodBuilder getterBuilder)
        {
            var propBuilder = builder.DefineProperty(propertyInfo.Name, propertyInfo.Attributes, propertyInfo.PropertyType, new Type[0]);

            if(setterBuilder != null)
                propBuilder.SetSetMethod(setterBuilder);

            if(getterBuilder != null)
                propBuilder.SetGetMethod(getterBuilder);
        }

        private static void BuildEventForEventInfo(TypeBuilder builder, EventInfo eventInfo)
        {
            // Generate fields for the event handlers
            var fbuilder = builder.DefineField("_" + eventInfo.Name.ToLowerInvariant(), eventInfo.EventHandlerType, FieldAttributes.Private | FieldAttributes.InitOnly);
            
            var addMethod = builder.DefineMethod("add_" + eventInfo.Name,
                MethodAttributes.Public | MethodAttributes.Final |
                MethodAttributes.HideBySig | MethodAttributes.SpecialName |
                MethodAttributes.NewSlot | MethodAttributes.Virtual,
                typeof(void),new []{eventInfo.EventHandlerType});

            var bodyGenerator = addMethod.GetILGenerator();
            CreateAddRemoveHander(bodyGenerator, true, eventInfo.EventHandlerType, fbuilder);

            var removeMethod = builder.DefineMethod("remove_" + eventInfo.Name,
                MethodAttributes.Public | MethodAttributes.Final |
                MethodAttributes.HideBySig | MethodAttributes.SpecialName |
                MethodAttributes.NewSlot | MethodAttributes.Virtual,
                typeof(void), new[] { eventInfo.EventHandlerType });

            bodyGenerator = removeMethod.GetILGenerator();
            CreateAddRemoveHander(bodyGenerator, false, eventInfo.EventHandlerType, fbuilder);

            // Check if the delegate type is of System.EventHandler type, if so add 'this' to the argument stack
            var isEventHandler =  
                eventInfo.EventHandlerType == typeof(EventHandler) || 
                (eventInfo.EventHandlerType.IsGenericType && 
                eventInfo.EventHandlerType.GetGenericTypeDefinition() == typeof(EventHandler<>));

            var paramTypes = eventInfo.EventHandlerType.GetDelegateParameterTypes();
            var returnType = eventInfo.EventHandlerType.GetDelegateReturnType();
            if (isEventHandler)
                paramTypes = paramTypes.Skip(1).ToArray();

            // Create an invocation method
            var invokeMethod = builder.DefineMethod(
                "invoke_" + eventInfo.Name, 
                MethodAttributes.Public,
                returnType,
                paramTypes);

            bodyGenerator = invokeMethod.GetILGenerator();             
            var invokeMethodInfo = eventInfo.EventHandlerType.GetMethod("Invoke");

            var noSubscribersLabel = bodyGenerator.DefineLabel();
            var returnLabel = bodyGenerator.DefineLabel();
            var localField = bodyGenerator.DeclareLocal(eventInfo.EventHandlerType);

            bodyGenerator.Emit(OpCodes.Ldarg_0);            // Load 'this'
            bodyGenerator.Emit(OpCodes.Ldfld, fbuilder);    // Load this._eventhandler to local object
            bodyGenerator.Emit(OpCodes.Stloc, localField);  
            bodyGenerator.Emit(OpCodes.Ldloc, localField);  
            bodyGenerator.Emit(OpCodes.Ldnull);
            bodyGenerator.Emit(OpCodes.Beq, noSubscribersLabel); // Return if the handler is null

            bodyGenerator.Emit(OpCodes.Ldloc, localField);  

            if (isEventHandler)
            {
                bodyGenerator.Emit(OpCodes.Ldarg_0);
            }

            // Load all arguments
            for (var i = 0; i < paramTypes.Length; i++)
            {
                bodyGenerator.Emit(OpCodes.Ldarg, i + 1);   
            }

            // Call invoke on the handler
            bodyGenerator.Emit(OpCodes.Call, invokeMethodInfo);
            bodyGenerator.Emit(OpCodes.Br, returnLabel);

            bodyGenerator.MarkLabel(noSubscribersLabel);
            if (returnType != typeof(void))
            {
                if(returnType.IsValueType)
                {
                    var getTypeMethod = typeof(Type).GetMethod("GetTypeFromHandle", new [] { typeof(RuntimeTypeHandle)});
                    bodyGenerator.Emit(OpCodes.Ldtoken, returnType);
                    bodyGenerator.Emit(OpCodes.Call, getTypeMethod);
                    var createInstanceMethod = typeof(Activator).GetMethod("CreateInstance", new[] { typeof(Type) });
                    bodyGenerator.Emit(OpCodes.Call, createInstanceMethod);
                } 
                else
                {
                    bodyGenerator.Emit(OpCodes.Ldnull);
                }
            }

            bodyGenerator.MarkLabel(returnLabel);
            bodyGenerator.Emit(OpCodes.Ret);
        }

        private static void CreateAddRemoveHander(ILGenerator generator, bool add, Type handlerType, FieldBuilder handlerField)
        {
            // The following code is derived from a decompiled C# program, it basically adds a passed eventhandler to the defined field     
            var loopstartLabel = generator.DefineLabel();
            var objectType = typeof(object);
            var threadingInterlockedCompareExchangeMethod = 
                typeof(Interlocked).GetMethod("CompareExchange", new[] { objectType.MakeByRefType(), objectType, objectType });

            //.maxstack 3
            //.locals init (
            //    [0] class [mscorlib]System.EventHandler`1<class IRPC.TCP.Test.EventTest.EventOccurredEventArgs>,
            //    [1] class [mscorlib]System.EventHandler`1<class IRPC.TCP.Test.EventTest.EventOccurredEventArgs>,
            //    [2] class [mscorlib]System.EventHandler`1<class IRPC.TCP.Test.EventTest.EventOccurredEventArgs>
            //)
            var handler0 = generator.DeclareLocal(handlerType);
            var handler1 = generator.DeclareLocal(handlerType);
            var handler2 = generator.DeclareLocal(handlerType);

            //IL_0000: ldarg.0
            //IL_0001: ldfld class [mscorlib]System.EventHandler`1<class IRPC.TCP.Test.EventTest.EventOccurredEventArgs> IRPC.TCP.Test.EventTest.EventTestObject::EventOccurred
            //IL_0006: stloc.0

            generator.Emit(OpCodes.Ldarg_0);
            generator.Emit(OpCodes.Ldfld, handlerField);
            generator.Emit(OpCodes.Stloc, handler0);

            //    IL_0007: ldloc.0
            //    IL_0008: stloc.1
            //    IL_0009: ldloc.1
            //    IL_000a: ldarg.1
            //    IL_000b: call class [mscorlib]System.Delegate [mscorlib]System.Delegate::Combine(class [mscorlib]System.Delegate, class [mscorlib]System.Delegate)
            //    IL_0010: castclass class [mscorlib]System.EventHandler`1<class IRPC.TCP.Test.EventTest.EventOccurredEventArgs>
            //    IL_0015: stloc.2
            //    IL_0016: ldarg.0
            //    IL_0017: ldflda class [mscorlib]System.EventHandler`1<class IRPC.TCP.Test.EventTest.EventOccurredEventArgs> IRPC.TCP.Test.EventTest.EventTestObject::EventOccurred
            //    IL_001c: ldloc.2
            //    IL_001d: ldloc.1
            //    IL_001e: call !!0 [mscorlib]System.Threading.Interlocked::CompareExchange<class [mscorlib]System.EventHandler`1<class IRPC.TCP.Test.EventTest.EventOccurredEventArgs>>(!!0&, !!0, !!0)
            //    IL_0023: stloc.0
            //    IL_0024: ldloc.0
            //    IL_0025: ldloc.1
            //    IL_0026: bne.un.s IL_0007

            generator.MarkLabel(loopstartLabel);
            generator.Emit(OpCodes.Ldloc, handler0);
            generator.Emit(OpCodes.Stloc, handler1);
            generator.Emit(OpCodes.Ldloc, handler1);
            generator.Emit(OpCodes.Ldarg_1);

            MethodInfo delegateMethod;
            var delegateType = typeof(Delegate);
            if(add)            
                delegateMethod = delegateType.GetMethod("Combine", new[] { delegateType, delegateType });
            else
                delegateMethod = delegateType.GetMethod("Remove", new[] { delegateType, delegateType });
            
            generator.Emit(OpCodes.Call, delegateMethod);
            generator.Emit(OpCodes.Castclass, handlerType);
            generator.Emit(OpCodes.Stloc, handler2);
            generator.Emit(OpCodes.Ldarg_0);
            generator.Emit(OpCodes.Ldflda, handlerField);
            generator.Emit(OpCodes.Ldloc, handler2);
            generator.Emit(OpCodes.Ldloc, handler1);
            generator.Emit(OpCodes.Call, threadingInterlockedCompareExchangeMethod);
            generator.Emit(OpCodes.Stloc, handler0);
            generator.Emit(OpCodes.Ldloc, handler0);
            generator.Emit(OpCodes.Ldloc, handler1);
            generator.Emit(OpCodes.Bne_Un_S, loopstartLabel);

            //IL_0028: ret
            generator.Emit(OpCodes.Ret);
        }
               

        #endregion

    }
}
