﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace IRPC.TCP
{
    public class TCPInterfacingServer : TCPInterfacingConnectorBase, IInterfacingServer
    {
        private const int QueueLength = 10;

        private Socket _socket;
        private Thread _clientAcceptorThread;
        private CancellationTokenSource _clientAcceptorThreadTokenSource;
        
        public void StartServer(Uri hostUri)
        {
            // Parse URI
            if (hostUri.Scheme != "net.tcp")
                throw new ArgumentException("Invalid URI protocol should be net.tcp");

            if (hostUri.Host != "localhost" && hostUri.Host != "127.0.0.1")
                throw new ArgumentException("Invalid hostname should be localhost or 127.0.0.1");

            var port = hostUri.Port;

            lock (this)
            {
                // Check if running
                if (_socket != null)
                    return;

                _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                _socket.Bind(new IPEndPoint(IPAddress.Any, port));
                _socket.Listen(QueueLength);

                _clientAcceptorThread = new Thread(ClientAcceptorWorker);
                _clientAcceptorThreadTokenSource = new CancellationTokenSource();
                _clientAcceptorThread.Start(_clientAcceptorThreadTokenSource.Token);
            }
        }

        public void StopServer()
        {
            if (_socket == null)
                return;

            // Cancel thread
            _clientAcceptorThreadTokenSource.Cancel();

            // Wait for thread to finish
            _clientAcceptorThread.Join();
        }
       
        private void ClientAcceptorWorker(object cancelToken)
        {
            var token = (CancellationToken)cancelToken;
            while(true)
            {
                if (token.IsCancellationRequested)
                    break;
                
                var acceptTask = Task.Factory.FromAsync<Socket>(_socket.BeginAccept, _socket.EndAccept, null);
                try
                {
                    acceptTask.Wait(token);
                } 
                catch(OperationCanceledException)
                {
                    break;
                }  
                catch(AggregateException e)
                {
                    var nonSocketExceptions = e.InnerExceptions.Where(x => !(x is SocketException));
                    if (nonSocketExceptions.Any())
                        throw new AggregateException(nonSocketExceptions);
                    break;
                }

                OnConnectionEstablished(acceptTask.Result);
            }

            lock (this)
            {
                // Check if running
                if (_socket == null)
                    return;
                
                // Close socket
                _socket.Close();

                // clear variables
                _socket = null;
                _clientAcceptorThread = null;
                _clientAcceptorThreadTokenSource = null;
            }
        }      
    }
}
