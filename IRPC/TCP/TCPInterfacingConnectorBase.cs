﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using IRPC.TCP.Manifests;
using IRPC.TCP.Extensions;

namespace IRPC.TCP
{
    internal class RemoteSubscriber
    {
        internal Type RemoteType { get; set; }

        internal Action<Socket, IList<Serializer>> ConstructBridgeAction { get; set; }
    }

    internal class LocalPublisher
    {
        internal Type LocalType { get; set; }

        internal Action<Socket, IList<Serializer>> ConstructReceiverAction { get; set; }
    }

    public abstract class TCPInterfacingConnectorBase : IInterfacingConnector
    {
        private const int ReceiveTimeout = 3000;
        private const int QueueLength = 10;
        private const string SerializableTypeHelp = "Either apply the SerializableAttribute to the type or create a custom IInterfacingSerializer<TType> and register it using the RegisterSerializer<TType,TSerializer>() function.";

        private readonly IList<IInterfacingBridge> _activeReceivers;

        internal IList<RemoteSubscriber> RemoteSubscribers { get; private set; }
        internal IList<LocalPublisher> LocalPublishers { get; private set; }
        internal IList<Serializer> Serializers { get; private set; } 

        protected TCPInterfacingConnectorBase()
        {
            RemoteSubscribers = new List<RemoteSubscriber>();
            LocalPublishers = new List<LocalPublisher>();
            Serializers = new List<Serializer>();

            _activeReceivers = new List<IInterfacingBridge>();
        }    

        public void RegisterInterface<TLocal>(TLocal localInterface, Action<IInterfacingBridge> onConnectionEstablished)
           where TLocal : class
        {
            // Check if TLocal and TRemote are PUBLIC interfaces
            var localType = typeof(TLocal);

            if (!localType.IsInterface)
                throw new InvalidOperationException("TLocal must be an interace");

            if (!localType.IsPublic)
                throw new InvalidOperationException("TLocal must have the public access modifier");

            // For all methods check involved types for serializability
            foreach(var method in localType.GetMethods())
            {
                if (method.IsSpecialName)
                    continue;

                foreach(var param in method.GetParameters())
                {
                    if (!param.ParameterType.IsSerializable() && !HasCustomSerializer(param.ParameterType))
                    {
                        throw new InvalidOperationException(
                            "Parameter '" + param.Name + "' of type '" + param.ParameterType + "' for method '" + method.Name + "' is not serializable.\n" + SerializableTypeHelp);                         
                    }
                }

                if (!method.ReturnType.IsSerializable() && !HasCustomSerializer(method.ReturnType))
                {
                    throw new InvalidOperationException(
                        "Return type of type '" + method.ReturnType + "' for method '" + method.Name + "' is not serializable.\n" + SerializableTypeHelp);
                }
            }

            // For all properties check involved types for serializability
            foreach (var prop in localType.GetProperties())
            {
                if(!prop.PropertyType.IsSerializable() && !HasCustomSerializer(prop.PropertyType))
                {
                    throw new InvalidOperationException(
                        "Property '" + prop.Name + "' of type '" + prop.PropertyType + "' is not serializable.\n" +  SerializableTypeHelp);
                }
            }

            // For all events check involved types for serializability
            foreach(var ev in localType.GetEvents())
            {
                var delegateParams = ev.EventHandlerType.GetDelegateParameterTypes();
                
                if(ev.EventHandlerType == typeof(EventHandler) || (ev.EventHandlerType.IsGenericType && ev.EventHandlerType == typeof(EventHandler<>)))
                {
                    delegateParams = delegateParams.Skip(1).ToArray();
                }
                
                foreach(var param in delegateParams)
                {
                    if(!param.IsSerializable() && !HasCustomSerializer(param))
                    {
                        throw new InvalidOperationException(
                            "Event '" + ev.Name + "' of type '" + ev.EventHandlerType + "' is not serializable, due to parameter of type '" + param.Name + "'.\n" +  SerializableTypeHelp);
                    }
                }

                var returnType = ev.EventHandlerType.GetDelegateReturnType();
                if(!returnType.IsSerializable() && !HasCustomSerializer(returnType))
                {
                    throw new InvalidOperationException(
                        "Event '" + ev.Name + "' of type '" + ev.EventHandlerType + "' is not serializable, due to return type of type '" + returnType.Name + "'.\n" +  SerializableTypeHelp);
                }
            }

            LocalPublishers.Add(new LocalPublisher
            {
                LocalType = typeof(TLocal),
                ConstructReceiverAction = (socket,serializers) =>
                {
                    var bridgeReceiver = new TCPInterfacingBridgeReceiver<TLocal>(socket, localInterface, serializers);
                    _activeReceivers.Add(bridgeReceiver);
                    onConnectionEstablished(bridgeReceiver);
                }
            });
        }

        public void RegisterInterface<TRemote>(Action<IInterfacingBridge<TRemote>> onConnectionEstablished)
            where TRemote : class
        {
            // Check if TLocal and TRemote are PUBLIC interfaces
            var remoteType = typeof(TRemote);

            if (!remoteType.IsInterface)
                throw new Exception("TRemote must be an interace");

            if (!remoteType.IsPublic)
                throw new Exception("TRemote must have the public access modifier");

            RemoteSubscribers.Add(new RemoteSubscriber
            {
                RemoteType = typeof(TRemote),
                ConstructBridgeAction = (socket, serializers) =>
                    onConnectionEstablished(new TCPInterfacingBridge<TRemote>(socket, serializers))
            });
        }

        public void RegisterSerializer<TType, TSerializer>() 
            where TSerializer : IInterfacingSerializer<TType> , new()
            where TType : class
        {
            var serializer = new TSerializer();
            Serializers.Add(new Serializer
            {
                SerializerType = serializer.GetType(),
                SerializedType = typeof(TType),
                DeserializeFunction = bytes => serializer.Deserialize(bytes),
                SerializeFunction = o => serializer.Serialize((TType) o)
            });
        }
        
        protected void OnConnectionEstablished(Socket socket)
        {
            Task.Factory.StartNew(s => SetupBridgeWorker(s as Socket),socket);
        }
        
        private void SetupBridgeWorker(Socket connectedSocket)
        {
            try
            {
                connectedSocket.ReceiveTimeout = ReceiveTimeout;
          
                // Send local interface manifest
                var localInterfaces = new InterfaceManifest 
                {
                    InterfaceTypeNames = LocalPublishers.Select(x => x.LocalType.AssemblyQualifiedName).ToList()
                };

                SendPacket(connectedSocket, localInterfaces);
                var remoteInterfaces = ReceivePacket<InterfaceManifest>(connectedSocket);
                                
                // Send local serializer manifest and receive remote one
                var localSerializers = new SerializerManifest
                {
                    SerializerTypeNames = Serializers.Select(x => x.SerializerType.AssemblyQualifiedName).ToList()
                };
                SendPacket(connectedSocket, localSerializers);
                var remoteSerializers = ReceivePacket<SerializerManifest>(connectedSocket);

                // Find common serializers
                var serializerTypes = remoteSerializers.SerializerTypes.Intersect(localSerializers.SerializerTypes);
                var serializers = Serializers.Where(x => serializerTypes.Any(y => x.SerializerType == y)).ToList();

                // Compile the list of remote subscribes and send a BridgeConstructionManifest
                var remoteTypes = RemoteSubscribers.Select(x=>x.RemoteType);
                var remotes = remoteInterfaces.InterfaceTypes.Where(remoteTypes.Contains);

                var localBridgeManifest = new BridgeConstructionManifest {
                    BridgeConstructs = remotes.Select(x => new BridgeConstruct { InterfaceName = x.AssemblyQualifiedName }).ToList() };

                SendPacket(connectedSocket, localBridgeManifest);
                var remoteBridgeManifest = ReceivePacket<BridgeConstructionManifest>(connectedSocket);
                
                // Setup sockets foreach of the requested connections
                foreach(var bridgeConstruct in remoteBridgeManifest.BridgeConstructs)
                {
                    var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    socket.Bind(new IPEndPoint(IPAddress.Any, 0));
                    socket.Listen(QueueLength);

                    bridgeConstruct.ReceiverPort = ((IPEndPoint)socket.LocalEndPoint).Port;
                    var localPublisher = LocalPublishers.Single(x => x.LocalType == bridgeConstruct.InterfaceType);

                    Task.Factory.StartNew(o =>
                    {
                        var s = (Socket)o;
                        var newSocket = s.Accept();
                        localPublisher.ConstructReceiverAction(newSocket, serializers);
                        s.Close();
                    }, socket);
                }

                SendPacket(connectedSocket, remoteBridgeManifest);
                localBridgeManifest = ReceivePacket<BridgeConstructionManifest>(connectedSocket);

                // Connect to all sockets
                foreach(var bridgeConstruct in localBridgeManifest.BridgeConstructs)
                {
                    var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    socket.Bind(new IPEndPoint(IPAddress.Any, 0));

                    var remoteSubcriber = RemoteSubscribers.Single(x => x.RemoteType == bridgeConstruct.InterfaceType);
                    var remoteAddress = new IPEndPoint(((IPEndPoint)connectedSocket.RemoteEndPoint).Address, bridgeConstruct.ReceiverPort);

                    Task.Factory.StartNew(o =>
                    {
                        var s = (Socket)o;
                        s.Connect(remoteAddress);
                        remoteSubcriber.ConstructBridgeAction(s, serializers);
                    }, socket);
                }
            }
            finally
            {
                try
                {
                    // Make sure socket was closed
                    connectedSocket.Close();
                }
                catch (SocketException e)
                {
                    if (e.SocketErrorCode == SocketError.TimedOut)
                        Console.WriteLine("Connection Timeout");
                }
            }
        }

        private bool HasCustomSerializer(Type type)
        {
            return Serializers.Any(x => x.SerializedType == type);
        }

        private static void SendPacket<T>(Socket socket, T data)
        {
            var serializer = new DataContractSerializer(typeof(T));

            byte[] dataBytes;
            // Serialize data
            using(var ms = new MemoryStream())
            {
                ms.Position = sizeof(int);
                serializer.WriteObject(ms, data);
                dataBytes = ms.ToArray();
            }
            
            Buffer.BlockCopy(BitConverter.GetBytes(dataBytes.Length),0,dataBytes,0,sizeof(int));
            socket.Send(dataBytes);
        }

        private static T ReceivePacket<T>(Socket socket)
        {
            var serializer = new DataContractSerializer(typeof(T));

            // Read size int
            var sizeBytes = new byte[4];
            var receivedBytes = 0;
            while(receivedBytes < sizeof(int))
            {
                receivedBytes += socket.Receive(sizeBytes,receivedBytes,sizeBytes.Length-receivedBytes,SocketFlags.None);
            }

            // Read packet
            var packetLength = BitConverter.ToInt32(sizeBytes, 0) - sizeof(int);
            var packetBuffer = new byte[packetLength];
            receivedBytes = 0;
            while (receivedBytes < packetLength)
            {
                receivedBytes += socket.Receive(packetBuffer, receivedBytes, packetBuffer.Length - receivedBytes, SocketFlags.None);
            }

            // Deserialize
            using(var ms = new MemoryStream(packetBuffer))
            {
                return (T)serializer.ReadObject(ms);
            }
        }
        
    }
}
